package ru.pflb.autotest.selenium;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.PickleEventWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.pflb.autotest.selenium.utils.CustomLogger;
import ru.pflb.autotest.selenium.utils.DriverManager;

@CucumberOptions(features = "src/test/resources"
        , glue = "ru.pflb.autotest.selenium.stepdefinitions")
public class CucumberRunner {
    private TestNGCucumberRunner runner;

    @BeforeClass
    public void setUp() {
        runner = new TestNGCucumberRunner(this.getClass());
    }

    @AfterClass
    public static void tearDown() {
        CustomLogger.info("Завершаем выполнение теста");
        DriverManager.getInstance().exit();
    }

    @DataProvider
    public Object[][] features() {
        Object[][] scenarios = runner.provideScenarios();
        ((PickleEventWrapper) scenarios[0][0]).getPickleEvent();
        return scenarios;
    }

    @Test(dataProvider = "features")
    public void feature(PickleEventWrapper pickleEventWrapper, CucumberFeatureWrapper cucumberFeatureWrapper) throws Throwable {
        runner.runScenario(pickleEventWrapper.getPickleEvent());
    }
}
