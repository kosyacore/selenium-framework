package ru.pflb.autotest.selenium.stepdefinitions;

import cucumber.api.java.ru.Пусть;
import io.qameta.allure.Step;
import ru.pflb.autotest.selenium.exception.ElementNotFoundException;
import ru.pflb.autotest.selenium.page.CreateMessagePage;
import ru.pflb.autotest.selenium.page.MainPage;
import ru.pflb.autotest.selenium.page.MessagesPage;
import ru.pflb.autotest.selenium.utils.CustomLogger;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class MessageSteps {
    @Step("Создано новое письмо")
    @Пусть("создано новое письмо")
    public static void createNewMessage(List<Map<String, String>> messageInfo) {
        CustomLogger.info("Открываем страницу создания нового письма");
        new MainPage()
                .goCreateMessage();
        CustomLogger.info(String.format("Заполняем поля значениями: { Получатель: \"%s\" | Тема: \"%s\" | Сообщение: \"%s\" }", messageInfo.get(0).get("Получатель"), messageInfo.get(0).get("Тема"), messageInfo.get(0).get("Сообщение")));
        new CreateMessagePage()
                .setSenderMail(messageInfo.get(0).get("Получатель"))
                .setSubject(messageInfo.get(0).get("Тема"))
                .setMessage(messageInfo.get(0).get("Сообщение"))
                .closeWindow();
    }

    @Step("Проверка наличия письма в черновиках")
    @Пусть("проверим наличие письма в черновиках")
    public static void checkDraftMessage(Map<String, String> messageInfo) {
        MessagesPage messagesPage = new MessagesPage();
        checkMessageSubject(messagesPage.getMessageSubject(), messageInfo.get(("Тема")));
        checkMessageBody(messagesPage.getMessageBody(), messageInfo.get("Сообщение"));
        CustomLogger.info(String.format("Письмо с темой \"%s\" и сообщением \"%s\" найдено", messageInfo.get("Тема"), messageInfo.get("Сообщение")));
    }

    @Step("Открываем сообщение в черновиках с темой письма {0}")
    @Пусть("открыть сообщение в черновиках с темой письма {string}")
    public static void clickOnMessage(String messageSubject) {
        CustomLogger.info(String.format("Ищем письмо с темой \"%s\"", messageSubject));
        new MessagesPage().getMessageBySubject(messageSubject);
        CustomLogger.info(String.format("Сообщение с темой \"%s\" найдено", messageSubject));
    }

    @Step("Проверяем, что {0} письма является {0}")
    @Пусть("проверим элементы письма")
    public static void checkMessageInfo(Map<String, String> messageInfo) {
        CreateMessagePage createMessagePage = new CreateMessagePage();
        if (messageInfo.containsKey("Получатель")) {
            CustomLogger.info(String.format("Проверяем адрес получателя письма \"%s\"", messageInfo.get("Получатель")));
            String messageRecipient = createMessagePage.getSenderMail();
            if (messageRecipient.equals(messageInfo.get("Получатель")))
                CustomLogger.info(String.format("Адрес получателя письма ожидаемо равен \"%s\"", messageInfo.get("Получатель")));
            else
                CustomLogger.warn(String.format("Адрес получателя письма равен \"%s\", ожидалось: \"%s\"", messageRecipient, messageInfo.get("Получатель")));
        } else if (messageInfo.containsKey("Тема")) {
            CustomLogger.info(String.format("Проверяем тему письма \"%s\"", messageInfo.get("Тема")));
            String messageSubject = createMessagePage.getSubject();
            if (messageSubject.equals(messageInfo.get("Тема")))
                CustomLogger.info(String.format("Тема письма ожидаемо равна \"%s\"", messageInfo.get("Тема")));
            else
                CustomLogger.warn(String.format("Тема письма равен \"%s\", ожидалось: \"%s\"", messageSubject, messageInfo.get("тема")));
        } else if (messageInfo.containsKey("Сообщение")) {
            CustomLogger.info(String.format("Проверяем сообщение письма \"%s\"", messageInfo.get("Сообщение")));
            String messageBody = createMessagePage.getMessage();
            if (messageBody.equals(messageInfo.get("Сообщение")))
                CustomLogger.info(String.format("Сообщение письма ожидаемо равно \"%s\"", messageInfo.get("Сообщение")));
            else
                CustomLogger.warn(String.format("Сообщение письма равно \"%s\", ожидалось: \"%s\"", messageBody, messageInfo.get("Сообщение")));
        } else {
            CustomLogger.error("Не верно указан параметр для проверки");
            throw new ElementNotFoundException("Не верно указан параметр для проверки");
        }
    }

    @Step("Проверяем, что отправленное письмо не отображается в черновиках")
    @Пусть("проверим, что отправленное письмо не отображается в черновиках")
    public static void checkMessageNotExist(Map<String, String> messageInfo) {
        MessagesPage messagesPage = new MessagesPage();
        boolean isSubjectPresent = checkMessageWithSubjectExist(messagesPage.getMessageSubject(), messageInfo.get(("Тема")));
        boolean isMessageBodyPresent = checkMessageWithBodyExist(messagesPage.getMessageBody(), messageInfo.get("Сообщение"));
        if (!isSubjectPresent & !isMessageBodyPresent)
            CustomLogger.info(String.format("Письмо с темой \"%s\" и сообщением \"%s\" ожидаемо не найдено", messageInfo.get("Тема"), messageInfo.get("Сообщение")));
        else
            CustomLogger.warn(String.format("Письмо с темой \"%s\" и сообщением \"%s\" неожиданно найден", messageInfo.get("Тема"), messageInfo.get("Сообщение")));
    }

    @Step("Отправляем письмо")
    @Пусть("отправить письмо")
    public static void sendMessage() {
        CustomLogger.info("Отправляем письмо");
        new CreateMessagePage().sendMessage();
    }

    @Step("Переходим в {0}")
    @Пусть("перейти в {string}")
    public static void selectMenuItem(String menuItem) {
        CustomLogger.info(String.format("Переходим на страницу \"%s\"", menuItem));
        new MainPage().selectItem(menuItem);
    }

    @Step("Проверяем наличие письма в отправленных")
    @Пусть("проверим наличие письма в отправленных")
    public static void checkSentMessage(Map<String, String> messageInfo) {
        CustomLogger.info("Проверяем наличие письма в отправленных");
        MessagesPage messagesPage = new MessagesPage();
        checkMessageSubject(messagesPage.getMessageSubject(), messageInfo.get(("Тема")));
        checkMessageBody(messagesPage.getMessageBody(), messageInfo.get("Сообщение"));
        CustomLogger.info(String.format("Письмо с темой \"%s\" и сообщением \"%s\" найдено", messageInfo.get("Тема"), messageInfo.get("Сообщение")));
    }

    private static String checkMessageSubject(List<String> messagesSubject, String subjectToFind) {
        return messagesSubject.stream().filter(e -> e.equalsIgnoreCase(subjectToFind)).findFirst().orElseThrow(() -> new NoSuchElementException(String.format("Письмо с темой \"%s\" не найдено", subjectToFind)));
    }

    private static String checkMessageBody(List<String> messagesBody, String bodyToFind) {
        return messagesBody.stream().filter(e -> e.equalsIgnoreCase(bodyToFind)).findFirst().orElseThrow(() -> new NoSuchElementException(String.format("Письмо с сообщением \"%s\" не найдено", bodyToFind)));
    }

    private static boolean checkMessageWithSubjectExist(List<String> messagesSubject, String subjectToFind) {
        return messagesSubject.stream().anyMatch(e -> e.equalsIgnoreCase(subjectToFind));
    }

    private static boolean checkMessageWithBodyExist(List<String> messagesSubject, String subjectToFind) {
        return messagesSubject.stream().anyMatch(e -> e.equalsIgnoreCase(subjectToFind));
    }
}
