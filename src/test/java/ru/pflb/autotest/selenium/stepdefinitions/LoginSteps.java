package ru.pflb.autotest.selenium.stepdefinitions;

import cucumber.api.java.ru.Пусть;
import io.qameta.allure.Step;
import ru.pflb.autotest.selenium.page.LoginPage;
import ru.pflb.autotest.selenium.page.MainPage;
import ru.pflb.autotest.selenium.utils.Credentials;
import ru.pflb.autotest.selenium.utils.CustomLogger;

public class LoginSteps {
    @Step("Пользователь открыл сайт {0}")
    @Пусть("пользователь открыл сайт {string}")
    public void openUrl(String url) {
        CustomLogger.info(String.format("Открываем URL: \"%s\"", url));
        new LoginPage()
                .open(url);
    }

    @Step("Выполнена авторизация")
    @Пусть("выполнена авторизация")
    public void login() {
        String login = Credentials.getInstance().getLogin();
        String password = Credentials.getInstance().getPassword();
        CustomLogger.info(String.format("Выполняем авторизацию с логином: \"%s\" и паролем: \"%s\"", login, password));
        new LoginPage()
                .setLogin(login)
                .pressContinue()
                .setPassword(password)
                .pressContinue();
    }

    @Step("Выполнен выход с сайта")
    @Пусть("выполнен выход с сайта")
    public void logout() {
        CustomLogger.info("Выполняем выход с сайта");
        new MainPage()
                .clickProfile()
                .clickExitButton();
    }
}
