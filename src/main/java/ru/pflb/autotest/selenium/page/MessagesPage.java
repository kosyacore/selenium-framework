package ru.pflb.autotest.selenium.page;

import org.openqa.selenium.WebElement;
import ru.pflb.autotest.selenium.utils.CustomLogger;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class MessagesPage extends AbstractPage {
    private String messageSubjectXpath = "//tr[@class='zA yO']//div[@class='y6']//span[@data-thread-id] | //span[@class='bqe']";
    private String messageBody = "//tr[@class='zA yO']//span[@class='y2'] | //span[@class='y2']";

    public MessagesPage() {
        waitForLoadFinished();
    }

    public List<String> getMessageSubject() {
        return driver.findElementsByXpath(messageSubjectXpath)
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getMessageBody() {
        return driver.findElementsByXpath(messageBody)
                .stream()
                .map(WebElement::getText)
                .map(e -> e.replaceFirst("- \n", "").trim())
                .collect(Collectors.toList());
    }

    public void getMessageBySubject(String messageSubject) {
        driver.findElementsByXpath(messageSubjectXpath)
                .stream()
                .filter(e -> e.getText().equalsIgnoreCase(messageSubject))
                .findFirst()
                .orElseThrow(NoSuchElementException::new)
                .click();
    }

    @Override
    void waitForLoadFinished() {
        CustomLogger.debug("Ожидаем загрузку страницы");
        driver.elementCheckerWithText("//span[@class='yP boq']", "Сообщения");
    }
}
