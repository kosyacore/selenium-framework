package ru.pflb.autotest.selenium.page;

public class CreateMessagePage extends AbstractPage {
    private String senderEmailTextFieldXpath = "//textarea[@aria-label='Кому']";
    private String senderEmailLabelXpath = "//div[@class='aoD hl']//div[@class='oL aDm az9']//span[@email]";
    private String subjectMessageXpath = "//input[@name='subjectbox']";
    private String messageXpath = "//div[@role='textbox']";
    private String closeWindowXpath = "//img[@alt='Закрыть']";
    private String sendMessageButtonXpath = "//div[contains(@data-tooltip, 'Отправить')]";

    public CreateMessagePage setSenderMail(String mail) {
        driver.setText(senderEmailTextFieldXpath, mail);
        return this;
    }

    public CreateMessagePage setSubject(String subject) {
        driver.setText(subjectMessageXpath, subject);
        return this;
    }

    public CreateMessagePage setMessage(String message) {
        driver.setText(messageXpath, message);
        return this;
    }

    public String getSenderMail() {
        return driver.getAttributeValue(senderEmailLabelXpath, "email");
    }

    public String getSubject() {
        return driver.getAttributeValue(subjectMessageXpath, "value");
    }

    public String getMessage() {
        return driver.getText(messageXpath);
    }

    public CreateMessagePage sendMessage() {
        driver.click(sendMessageButtonXpath);
        return this;
    }

    public CreateMessagePage closeWindow() {
        driver.click(closeWindowXpath);
        return this;
    }

    @Override
    void waitForLoadFinished() {

    }
}
