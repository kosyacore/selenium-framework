package ru.pflb.autotest.selenium.page;

public class MainPage extends AbstractPage {
    private String createMessageButton = "//div[text()='Написать']";
    private String profileButton = "//a[contains(@aria-label, 'Аккаунт Google')]";
    private String exitButton = "//a[text()='Выйти']";

    public MainPage selectItem(String itemName) {
        driver.click(String.format("//a[@title='%s']", itemName));
        return this;
    }

    public MainPage goCreateMessage() {
        driver.click(createMessageButton);
        return this;
    }

    public MainPage clickProfile() {
        driver.click(profileButton);
        return this;
    }

    public MainPage clickExitButton() {
        driver.click(exitButton);
        return this;
    }

    @Override
    void waitForLoadFinished() {

    }
}
