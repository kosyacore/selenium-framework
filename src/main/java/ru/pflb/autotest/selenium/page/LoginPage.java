package ru.pflb.autotest.selenium.page;

public class LoginPage extends AbstractPage {
    private String loginFieldXpath = "//input[@type='email']";
    private String continueButton = "//span[text()='Далее']";
    private String passwordFieldXpath = "//input[@type='password']";

    public LoginPage open(String url) {
        driver.openUrl(url);
        return this;
    }

    public LoginPage setLogin(String login) {
         driver.setText(loginFieldXpath, login);
         return this;
    }

    public LoginPage pressContinue() {
        driver.click(continueButton);
        return this;
    }

    public LoginPage setPassword(String password) {
        driver.setText(passwordFieldXpath, password);
        return this;
    }

    @Override
    void waitForLoadFinished() {

    }
}
