package ru.pflb.autotest.selenium.page;

import ru.pflb.autotest.selenium.utils.DriverManager;

abstract class AbstractPage {
    DriverManager driver = DriverManager.getInstance();

    abstract void waitForLoadFinished();
}
