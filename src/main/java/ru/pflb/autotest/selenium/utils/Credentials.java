package ru.pflb.autotest.selenium.utils;

public class Credentials {
    private static Credentials credentials;

    private Credentials() {

    }

    public static Credentials getInstance() {
        if (credentials == null)
            return credentials = new Credentials();
        else
            return credentials;
    }

    public String getLogin() {
        return System.getProperty("login");
    }

    public String getPassword() {
        return System.getProperty("password");
    }
}
