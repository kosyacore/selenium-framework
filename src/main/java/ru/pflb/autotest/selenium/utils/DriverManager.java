package ru.pflb.autotest.selenium.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.pflb.autotest.selenium.config.Config;
import ru.pflb.autotest.selenium.exception.ElementNotFoundException;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    private WebDriverWait wait;
    private WebDriver driver;
    private static DriverManager driverManager;

    private DriverManager() {
        CustomLogger.debug("ChromeDriver не существует. Создаем новый...");
        driver = new ChromeDriver(Config.getChromeDriverConfiguration());
        driver.manage().timeouts().implicitlyWait(Config.getWaitTimeout(), TimeUnit.SECONDS);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, Config.getWaitTimeout(), Config.getSleep());
    }

    public static DriverManager getInstance() {
        if (driverManager == null) {
            driverManager = new DriverManager();
        }
        return driverManager;
    }

    public void openUrl(String url) {
        CustomLogger.debug(String.format("Открываем URL: \"%s\"", url));
        driver.get(url);
    }

    public WebElement findElementByXpath(String xPath) {
        try {
            CustomLogger.debug(String.format("Выполняем поиск элемента по xPath: \"%s\"", xPath));
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
            return driver.findElement(By.xpath(xPath));
        } catch (Exception e) {
            CustomLogger.error(String.format("Элемент с xPath \"%s\" не найден", xPath));
            throw new NoSuchElementException(String.format("Элемент с xPath \"%s\" не найден", xPath), e.getCause());
        }
    }

    public List<WebElement> findElementsByXpath(String xPath) {
        try {
            CustomLogger.debug(String.format("Выполняем поиск элементов по xPath: \"%s\"", xPath));
            return driver.findElements(By.xpath(xPath));
        } catch (Exception e) {
            CustomLogger.error(String.format("Элемент с xPath \"%s\" не найден", xPath));
            throw new ElementNotFoundException(String.format("Элемент с xPath \"%s\" не найден", xPath), e.getCause());
        }
    }

    public void click(String xPath) {
        try {
            CustomLogger.debug(String.format("Выполняем клик по элементу с xPath: \"%s\"", xPath));
            findElementByXpath(xPath).click();
        } catch (Exception e) {
            CustomLogger.error(String.format("Невозможно осуществить клик по элементу с xPath \"%s\"", xPath));
            throw new ElementNotInteractableException(String.format("Невозможно осуществить клик по элементу с xPath \"%s\"", xPath), e.getCause());
        }
    }

    public void setText(String xPath, String text) {
        try {
            CustomLogger.debug(String.format("Вводим текст \"%s\" в элемент с xPath: \"%s\"", text, xPath));
            findElementByXpath(xPath).sendKeys(text);
        } catch (Exception e) {
            CustomLogger.error(String.format("Невозможно ввести текст \"%s\" в элемент с xPath \"%s\"", text, xPath));
        }
    }

    public String getText(String xPath) {
        try {
            CustomLogger.debug(String.format("Получаем текст из элемента с xPath: \"%s\"", xPath));
            return findElementByXpath(xPath).getText();
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    public void elementCheckerWithText(String xPath, String text) {
        try {
            wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(xPath), text));
        } catch (Exception e) {
//            CustomLogger.warn("Ошибка ожидания загрузки страницы");
        }
    }

    public String getAttributeValue(String xPath, String attribute) {
        return driver.findElement(By.xpath(xPath)).getAttribute(attribute);
    }

    public String getPageSource() {
        return driver.getPageSource();
    }

    public byte[] takeScreenshot() {
        try {
            return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        } catch (Exception e) {
            return new byte[0];
        }
    }

    public void exit() {
        CustomLogger.debug("Выполняем закрытие драйвера");
        driver.close();
    }
}
