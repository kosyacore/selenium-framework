package ru.pflb.autotest.selenium.utils;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.RuntimeErrorException;


public class CustomLogger {
    private static final Logger logger;

    static {
        logger = LoggerFactory.getLogger(CustomLogger.class);
    }

    private CustomLogger() {
    }

    public static void debug(String message) {
        logger.debug(message);
    }

    @Step("{0}")
    public static void info(String message) {
        logger.info(message);
    }

    @Step("Получена ошибка {0}")
    public static void warn(String message) {
        logger.warn(message);
        attachPageSource();
        attachScreenshot();
    }

    @Step("{0}")
    public static void error(String message) {
        logger.error(message);
        attachPageSource();
        attachScreenshot();
    }

    @Attachment(value = "Page screenshot", type = "image/jpg")
    private static byte[] attachScreenshot() {
        return DriverManager.getInstance().takeScreenshot();
    }

    @Attachment(value = "Page source", type = "text/xml")
    private static byte[] attachPageSource() {
        try {
            return DriverManager.getInstance().getPageSource().getBytes();
        } catch (Exception e) {
            return new byte[0];
        }
    }
}
