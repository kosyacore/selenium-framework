package ru.pflb.autotest.selenium.config;

import org.openqa.selenium.chrome.ChromeOptions;

import java.util.logging.Level;

public class Config {
    private static final long WAIT_TIMEOUT = 3;
    private static final long SLEEP = 250;

    private static final String CHROMEDRIVER_PATH = "/Users/user/dev/armdriver";
    private static final String CHROMEDRIVER_LOGS = "true";
    private static final Level SELENIUM_LOGS = Level.OFF;

    public static long getWaitTimeout() {
        return WAIT_TIMEOUT;
    }

    public static long getSleep() {
        return SLEEP;
    }

    public static String getChromeDriverPath() {
        return CHROMEDRIVER_PATH;
    }

    public static String getChromeDriverLogs() {
        return CHROMEDRIVER_LOGS;
    }

    public static Level getSeleniumLogs() {
        return SELENIUM_LOGS;
    }

    public static ChromeOptions getChromeDriverConfiguration() {
        System.setProperty("webdriver.chrome.driver", getChromeDriverPath());
        System.setProperty("webdriver.chrome.silentOutput", getChromeDriverLogs());
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(getSeleniumLogs());
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen", "-disable-infobars");
        return chromeOptions;
    }
}
